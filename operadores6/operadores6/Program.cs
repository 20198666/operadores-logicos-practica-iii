﻿using System;
/* Alejandro Bruno 2019-8666
 Crea un programa que pida al usuario dos numeros enteros. Si el segundo no es cero, mostrara el resultado de dividir entre el primero y
 el segundo. Por el contrario, si el sugundo numero es cero, escribira "Error: No se puede dividir entre cero".*/
namespace operadores6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Programa para devidir.");
            int num1, num2, resul;

            Console.Write("Digite el primer numero: ");
            num1 = int.Parse(Console.ReadLine());

            Console.Write("Digite el segundo numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num2 == 0) 
            {
                Console.WriteLine("Error: No se puede dividir entre cero.");
            }
            else
            {
                resul = num1 / num2;
                Console.WriteLine("El resultado es: " + resul);
            }
            Console.ReadKey();
        }
    }
}
