﻿using System;
/* Alejandro Bruno 2019-8666
   Crear un programa que pida al usuario un numero entero y diga si es multiplo de 4 o 5.*/
namespace operadores7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Programa para verificar los multiplo de 4 o 5");
            int num;
            Console.Write("Digite un numero: ");
            num = int.Parse(Console.ReadLine());

            if (num % 4 == 0 || num % 5 == 0) 
            {
                Console.WriteLine("Es multiplo de 4 o 5");
            }
           else
           {
                Console.WriteLine("No es multiplo de 4 o 5");
           }
            Console.ReadKey();                   
        }
    }
}
