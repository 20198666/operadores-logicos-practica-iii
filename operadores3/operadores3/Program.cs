﻿using System;

namespace operadores3
{
    class Program
    {
        static void Main(string[] args) 
            /* Alejandro bruno 2019-8666
             Crea un programa que pida al usuario dos numeros enteros y diga si el primero es multiplo del segundo*/
        {
            int num1;
            int num2;

            Console.Write("Ingrese el primer numero: ");
            num1 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese el segundo numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num1 % num2 == 0) 
            {
                Console.WriteLine("Es multiplo del segundo.");
            }
            else
            {
                Console.WriteLine("No es multiplo del segundo");
            }
            Console.ReadKey();



          
        }
    }
}
