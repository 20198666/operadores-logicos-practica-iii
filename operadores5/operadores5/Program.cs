﻿using System;
/* Alejandro Bruno 2019-8666
  Crea un programa que multiplique dos numeros enteros de la siguiente forma: pedira al usuario un numero. 
  Si el numero que se que teclee es 0, escribira en pantalla "El producto de cero es 0". Si se ha tecleado un numero distinto de cero,
  se pedira al usuario un segundo numero y se mostrara el producto de ambos.*/
namespace operadores5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Programa para multiplicar");
            int num1, num2, prod;

            Console.Write("Digite el primer numero: ");
            num1 = int.Parse(Console.ReadLine());

            if (num1 == 0)
            {
                Console.WriteLine("El producto de 0 es 0 ");
            }
            else
            {
                Console.Write("Digite el segundo numero: ");
                num2 = int.Parse(Console.ReadLine());
                prod = num1 * num2;
                Console.WriteLine("El producto es: " + prod);
            }
            Console.ReadKey();
        }
    }
}
