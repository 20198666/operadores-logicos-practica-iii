﻿using System;
/* Alejandro bruno
   Crea un programa que pida al usuario tres numero reales y muestre cual es el mayor de los tres.*/
namespace operadores9
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1, num2, num3,nalto;
            Console.WriteLine("Programa para determinar el mayor.");
            Console.Write("Digite el primer numero: ");
            num1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Digite el segundo numero: ");
            num2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Digite el tercero numero ");
            num3 = Convert.ToDouble(Console.ReadLine());

            if (num1 > num2 && num1>num3)
            {
                nalto=num1;
            }
            else if (num2>num1 && num2 > num3)
            {
                nalto = num2;
            }
            else
            {
                nalto = num3;
            }
            Console.WriteLine("El numero mayor es :" + nalto);

            Console.ReadKey();
        }
    }
}
