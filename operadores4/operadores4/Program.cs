﻿using System;

namespace operadores4
{
    class Program
    {
        static void Main(string[] args)
            /* Alejandro bruno 2019-8666
              Crea un programa que pida al usuario un numero entero. Si es multiplo de 10, se lo avisara al usuario y pedira un segundo numero,
              para decir a continuacion si este segundo numero tambien es multiplo de 10.*/
        {
            Console.WriteLine("Programa para calcular el mutiplo de 10");
            int num1, num2;

            Console.Write("Escriba un numero entero: ");
            num1 = int.Parse(Console.ReadLine());

            if (num1 % 10 == 0)  
            {
                Console.WriteLine("Es multiplo :) ");
                Console.Write("Escriba un segundo numero entero: ");
                num2 = int.Parse(Console.ReadLine());

                if (num2 % 10 == 0)  
                {
                    Console.WriteLine("Es multiplo :) ");
                }
                else
                {
                    Console.WriteLine("No es multiplo :(");
                }

            }
            else
            {
                Console.WriteLine("No es multiplo :(");
            }
            Console.ReadKey();                

            
        }
    }
}
