﻿using System;

namespace operadores
{
    class Program
    {
        static void Main(string[] args)
        {//Crea un programa que pida al usuario un numero entero y diga si es par.
            Console.WriteLine("Programa para determinar un numero par.");
            int nu1;

            Console.WriteLine("Digite un numero entero.");
            nu1 = int.Parse(Console.ReadLine());

            if (nu1 % 2 == 0) 
            {
                Console.WriteLine("Es par");
            }
            else
            {
                Console.WriteLine("Es impar");
            }
            Console.ReadKey();

            
            
        }
    }
}
