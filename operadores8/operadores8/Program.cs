﻿using System;
/* Alejandro Bruno 2019-8666
 Crea un programa que pida al usuario dos numero enteros y diga "Uno de los numeros es positivo", "Los dos numeros son positivos"
  o bien "Ninguno de los numeros es positivo", segun respondam*/
namespace operadores8
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2;

            Console.Write("Digite un numero: ");
            num1 = int.Parse(Console.ReadLine());

            Console.Write("Digite un numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num1 > 0 && num2 > 0)
            {
                Console.WriteLine("Los numeros son positivo");
            }
            else if(num1<0 && num2>0 || num1>0 && num2<0)
            {
                Console.WriteLine("Uno de los numeros es positivo");           
            }
            
            else 
            {
                Console.WriteLine("Ninguno de los numeros es positivo");
            }
            Console.ReadKey();
        }
    }
}

